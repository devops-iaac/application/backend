const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = 80;

app.use(bodyParser.urlencoded({ extended: false }));
app.use((req, res, next) => {
  const allowedOrigins = [ 'http://localhost:3000', process.env.origin || "http://localhost" ];
  const origin = req.headers.origin;
  console.log(origin);
  if (allowedOrigins.includes(origin)) {
       res.setHeader('Access-Control-Allow-Origin', origin);
  }
  // res.header('Access-Control-Allow-Origin', 'http://127.0.0.1:8020');
  // res.header('Access-Control-Allow-Methods', 'GET, OPTIONS');
  // res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
  // res.header('Access-Control-Allow-Credentials', true);
  return next();
});

const databank = {
  "batman":"Bruce Wayne",
  "superman":"Clarke Kent"
};

// Health check endpoint
app.get("/health", (req, res) => {
  res.status(200);
  res.send("healthy");
});

app.get("/superheroes/identify/:name", (req, res) => {
  let output = "";
  let inputName = req.params.name.toLowerCase();
  console.log(inputName)
  try {
    if (databank.hasOwnProperty(inputName)) {
      output=databank[inputName];
    }else {
      output="Is there a superhero with that name?";
    }
  } catch (e) { 
    res.send("Error");
  }
  res.json({ result: output });
});

app.listen(port, () => {
  console.log(`App listening on port ${port} !`);
});