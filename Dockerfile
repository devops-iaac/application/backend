FROM node:16-alpine as build
ARG origin_default='http://localhost'
ENV origin=$origin_default
WORKDIR /app
COPY package.json /app
RUN npm install
COPY . /app
EXPOSE 80
CMD ["npm", "start"]